require('dotenv').config();
const devConfig = {
    JWT_SECRET: process.env.JWT_SECRET_DEV,
    MONGO_URL: process.env.MONGO_URL_DEV,
};

const prodConfig = {
    JWT_SECRET: process.env.JWT_SECRET_PROD,
    MONGO_URL: process.env.MONGO_URL_DEV,
};

const defaultConfig = {
    PORT: process.env.PORT || 3000,
};

function envConfig(env) {
    switch (env) {
        case 'dev':
            return devConfig;
        case 'test':
            return testConfig;
        default:
            return prodConfig;
    }
}

module.exports = {
    ...defaultConfig,
    ...envConfig(process.env.NODE_ENV),
}