/**
* Configuration of the server middlewares.
*/

const bodyParser = require('body-parser');
const morgan = require('morgan');
const compression = require('compression');
const methodOverride = require('method-override');
const helmet = require('helmet');
const cors = require('cors');
const expressStatusMonitor = require('express-status-monitor');
const busboy = require('connect-busboy');
const isTest = process.env.NODE_ENV === 'test';
const isDev = process.env.NODE_ENV === 'development';

const app = (app) => {
    app.use(compression());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(busboy({ limits: { fileSize: 10 * 1024 * 1024 } }));
    app.use(helmet());
    app.use(cors());
    app.use(expressStatusMonitor());
    app.use(methodOverride());
    if (isDev && !isTest) {
        app.use(morgan('dev'));
    }
};

module.exports = app;