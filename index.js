// Transpile all code following this line with babel and use 'env' (aka ES6) preset.
require('babel-register')({
    presets: [ 'env' ]
})
const http = require('http');
const app = require('./api/app')
require('dotenv').config()
const port = process.env.PORT  || 3000;

const server = http.createServer(app);
server.listen(port);