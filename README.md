# express-rest-api

This the template project with webpack configured. also, Es6 enabled with babel transpilar.# express-rest-es6-enabled-apis

## Introducing webpack and Integrating it with Babel


As mentioned, ES6 modules allow the JavaScript developer to break their code up into manageable chunks, but the consequence of this is that those chunks have to be served up to the requesting browser, potentially adding dozens of additional HTTP requests back to the server — something we really ought to be looking to avoid. This is where webpack comes in.

webpack is a module bundler. Its primary purpose is to process your application by tracking down all its dependencies, then package them all up into one or more bundles that can be run in the browser. However, it can be far more than that, depending upon how it’s configured.

webpack configuration is based around four key components:

1 an entry point
2 an output location
3 loaders
4 plugins
5 Entry: This holds the start point of your application from where webpack can identify its dependencies.

Output: This specifies where you would like the processed bundle to be saved.

Loaders: These are a way of converting one thing as an input and generating something else as an output. They can be used to extend webpack’s capabilities to handle more than just JavaScript files, and therefore convert those into valid modules as well.

Plugins: These are used to extend webpack’s capabilities into other tasks beyond bundling — such as minification, linting and optimization.

To install webpack, run the following from your <ROOT> directory:

### npm install webpack webpack-cli --save-dev


webpack has the option of using either “development” or “production” modes. Setting mode: 'development' optimizes for build speed and debugging, whereas mode: 'production' optimizes for execution speed at runtime and output file size. 


## Transpiling : 

As mentioned earlier, loaders allow us to convert one thing into something else. In this case, we want ES6 converted into ES5. To do that, we’ll need a couple more packages:

### `npm install babel-loader babel-core --save-dev`

To utilize them, the webpack.config.js needs a module section adding to it after the output section,

```javascript
module.exports = {
entry: "./src/js/index.js",
output: {
    path: path.resolve(__dirname, "public/js"),
    filename: "bundle.js"
},
module: {
    rules: [
    {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
        loader: "babel-loader",
        options: {
            presets: ["babel-preset-env"]
        }
        }
    }
    ]
}
};
```

With that done, you can rerun this:

#### `npm run buildier, to establish the transpile parameters set in the .babelrc file.`

